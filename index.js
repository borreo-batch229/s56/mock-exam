function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

if(letter.length == 1){
    for (i = 0; i < sentence.length; i++) {
        if (sentence[i] == letter) {
            result += 1;
        }
    }
    return result;
}else{
    return undefined;
}
}

console.log(countLetter("a","apple austria"));


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
   var i, j;
   text = text.toLowerCase();
   for(i = 0; i < text.length; ++i) {
     for(j = i + 1; j < text.length; ++j) {
       if(text[i] === text[j]) {
         return false;
       }
     }
   }
   return true;
}
console.log(isIsogram("abcde"));

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
let x=0;

    if(age<13){
        return undefined;
    }else if((age>12 && age<22) || age>64){
        x = Math.round(price * 80) / 100
        return String(x);
    }else if(age>21 && age<65){
        x = Math.round(price*100) / 100
        return String(x);
    }

}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
   
    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let arrayNew = [];

    for(i=0; i< items.length; i++){
        if(items[i].stocks == 0){
                    arrayNew.push(items[i].category);

        }
    }
    return arrayNew;
}
console.log(
    findHotCategories([
     { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
     { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
     { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
     { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
     { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
     ])
);


function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:


    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    


let arrayB=[];

for(i=0; i< candidateA.length; i++){
    for(j=0;j< candidateB.length ;j++){
        if(candidateA[i]==candidateB[j]){
            arrayB.push(candidateA[i])
        }
    }

}
return arrayB;
}
 console.log(findFlyingVoters(['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m'],
    ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']))
module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};